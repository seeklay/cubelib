import cv2
import numpy
import os, itertools
import cubelib.types
import cubelib.proto.v47
import cubelib
import colormap
import json

def grc(brightness: int = 1):
    cs = os.urandom(3)
    while cs[0] + cs[1] + cs[2] < 250:
        cs = os.urandom(3)
    r, g, b = (min(255, int(c * brightness)) for c in cs)
    return r, g, b


SCALE = 5
OUTPUT_WIDTH, OUTPUT_HEIGHT = 128 * SCALE, 128 * SCALE
print('set output to',OUTPUT_HEIGHT,OUTPUT_HEIGHT)

# INPUT = open("v47_MapChunkBulk.dat")
# METADATA = json.load(open('v47_MapChunkBulk.dat.json'))
with open("v47_ChunkData.dat", 'rb') as f:
    DATA = f.read()
# METADATA = [(6, 24, 0), (6, 25, 15)]
METADATA = [(6, 25, 15)]

SKYLIGHT = True

print(f"Chunk backend {cubelib.world.__backend__!r}")


def collect_uniq_materials(chunk):
    materials = set()
    for y in range(0, 16):
        for z in range(0, 16):
            for x in range(0, 16):
                block = chunk[y][z][x]
                materials.add(cubelib.proto.v47.enums.Block[block.id:block.damage])
    return materials


print("Chunk columnts count", len(METADATA))

p = cubelib.proto.v47.ClientBound.Play.MapChunkBulk(SKYLIGHT, METADATA, DATA)
column = p.Columns[METADATA[0][:2]]

# column selection
print("Non-air chunks map", str(bin(column.bitmask)[2:].zfill(16)))

# exit()

# collect uniq materials
# uma = set()
# for chunk in chunks:
#     ma = collect_uniq_materials(chunk)
#     uma.update(ma)

# print(json.dumps(list([m[0] for m in uma]), indent=4))
# exit()

# Block(id, damaga) -> tuple(id, damage)
# chunks = [
#     [
#         [
#             [
#                 (block.id, block.damage) for block in xrow
#             ]
#             for xrow in zrow
#         ]
#         for zrow in yrow
#     ]
#     for yrow in chunks
# ]

def get_y_slice(chunk, y):
    return chunk[y]

def get_z_slice(chunk, z):
    z_slice = []
    for y in range(len(chunk)):
        row = []
        for x in range(len(chunk[y])):
            row.append(chunk[y][x][z])
        z_slice.append(row)
    return z_slice


def colorize(slice, pallete):
    plain = itertools.chain.from_iterable(slice)
    out = bytearray()
    pallete_o = dict()

    for block in plain:
        if isinstance(block, tuple):
            block = block[0]
        if block not in pallete:
            pallete[block] = grc(1.13)
        out += bytes(pallete[block])
        pallete_o[block] = pallete[block]
    return out, pallete_o


def extract_positions(rgb, color):
    ps = []
    for i in range(0, len(rgb), int(3 * (len(rgb) / 35))):
        try:
            index = rgb.index(color, i)
        except ValueError:
            return ps
        ps.append(index)
        return ps
    return ps
co = slice(None)



# def preview_column()


for off, chunk in enumerate(chunks[co]):
    for y in range(0, 16):
        # slice = [row[::-1] for row in get_z_slice(chunk, y)[-1::-1]]
        slice = get_y_slice(chunk, y)
        rgb_artefact, pallete = colorize(slice, colormap.block)

        scene = numpy.ndarray(shape=(16, 16, 3), buffer=rgb_artefact, dtype=numpy.uint8)
        scene = cv2.resize(scene, (OUTPUT_WIDTH, OUTPUT_HEIGHT), interpolation=cv2.INTER_NEAREST)

        legend = numpy.ndarray(shape=(OUTPUT_WIDTH, OUTPUT_HEIGHT, 3), dtype=numpy.uint8)
        legend.fill(255)

        biomes_artefact, _ = colorize(biomes, colormap.biome)
        biomes_map = numpy.ndarray(shape=(16, 16, 3), buffer=biomes_artefact, dtype=numpy.uint8)
        biomes_map = cv2.resize(biomes_map, (OUTPUT_WIDTH, OUTPUT_HEIGHT), interpolation=cv2.INTER_NEAREST)

        light_artefact, _ = colorize(get_y_slice(skylight[co][off], y), colormap.light)
        light_map = numpy.ndarray(shape=(16, 16, 3), buffer=light_artefact, dtype=numpy.uint8)
        light_map = cv2.resize(light_map, (OUTPUT_WIDTH, OUTPUT_HEIGHT), interpolation=cv2.INTER_NEAREST)

        scene = numpy.concatenate((scene, legend, biomes_map, light_map), axis=0)

        for i, b in enumerate(set(itertools.chain.from_iterable(biomes))):
            cv2.putText(
                scene, 
                f"{cubelib.proto.v47.enums.Biome(b)}",
                (OUTPUT_WIDTH//2 - 30, OUTPUT_WIDTH*2 - (20 * i) - 10),
                cv2.FONT_HERSHEY_PLAIN,
                2.33, 
                colormap.biome[b],
                2, 
                1
            )

        for i, p in enumerate(pallete.items()):
            pos = (
                3, 25 * (i + 1) + OUTPUT_HEIGHT
            )

            cv2.putText(
                scene, 
                f"{p[0]}:{cubelib.proto.v47.enums.Block[p[0]]}", 
                pos, 
                cv2.FONT_HERSHEY_PLAIN,
                2,
                p[1],
                2, 
                1
            )
            ps = extract_positions(bytearray(scene), bytes(p[1]))
            index = ps[0]
            index /= 3
            xx = int(index % (OUTPUT_WIDTH))
            xy = int(index // (OUTPUT_WIDTH))

            cv2.line(
                scene,
                (xx, xy),
                (pos[0] + 70 + (5 * i), pos[1] - 15),
                grc(1.4),
                3,
                1
            )
        
        cv2.putText(
            scene, 
            f"L{y + (off * 16)}", 
            (OUTPUT_WIDTH-50, OUTPUT_WIDTH + 25),
            cv2.FONT_HERSHEY_PLAIN,
            1.73, 
            (0,0,0),
            2, 
            1
        )

        cv2.putText(
            scene, 
            f"{cubelib.world.__backend__}",
            (OUTPUT_WIDTH//2 - 150, OUTPUT_WIDTH*2 - 10),
            cv2.FONT_HERSHEY_PLAIN,
            1.73, 
            (0,0,0),
            2, 
            1
        )

        cv2.imshow('a', scene[:,:,::-1]) 
        cv2.waitKey(1)
        print("Showing y-slice at offset y=", y + (off * 16))
        input()
