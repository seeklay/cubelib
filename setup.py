from setuptools import setup, find_packages
import cubelib

with open("PYPI.md", "r") as f:
    long_description = f.read()
    f.close()

setup(
    name = "cubelib",
    version = cubelib.version,    
    author = "seeklay",
    author_email='rudeboy@seeklay.icu',
    url = "https://gitlab.com/seeklay/cubelib",
    download_url = "https://gitlab.com/seeklay/cubelib",
    description = "python library for building/parsing minecraft protocol packets",
    long_description_content_type = "text/markdown",    
    long_description = long_description,    
    classifiers = [
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Topic :: Internet"
    ],
    platforms = 'OS Independent',
    license = "GNU General Public License v3.0",
    packages = find_packages(),
    entry_points = {
        'console_scripts': ['mcsr=cubelib.mcsr:entry_point'],
    },
    install_requires = [
        'NBT'
    ]
)