from . import ClientBound, ServerBound

name = 'Minecraft Java Edition protocol version 340'
version = 340
capabilities = [
    "chat"
]
