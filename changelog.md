# Revision 0.0.1-pre
* test client don't work and raising zlib.error (waiting for debug/rewrite)
* added file for exceptions `excs.py`
* some little bugs fixed

# Revision 0.0.2-pre (Nov 1st 2022)
* test client works for now...
* add compressing flag to packet structure
* add functionality to set compression flag in read functions
* cubelib package now exporting `buildPacket` method
* fix `_buildCompressedPacket` generates wrong data len field
 
# Version 0.1.0 (Nov 7th 2022)
* add compression flag in p.Night.build
* now exporting supported versions of proto as list
* actively work on examples
* next update will contain refactoring of `p.Packet` struct
* and change versioning type

# Version 1.0.1 (Dec 26th 2022)
* fix parsing v-ind packets (оказывается он не работал)
* move `Login` to `ClassicLogin`
* rewrite `resolve`

# Version 1.0.2 (Jan 5th 2023)  HNY23
* fix `readPacketsStream` read 1 packets and return (у меня даже вроде была уже такая проблема и я ее фиксил но после перемещения из `.functions` в `.p` видимо я снова ее сюда вернул) =]
* add `rrPacketsStream` new function
* make `DecoderException` more informatively on unexhausted buffer
* rewrite tests for new api version 
* add russian readme
* add examples readme
###### Old functionaliy was unchanged and compatibility not lost, so, v = 1.0.1 + 0.0.1

# Version 1.0.3-pre.1 (Jan 5th 2023)
* make `Packet` comparable
* add description for some types
* rewrite tests
* add `_bound()` method to the `Night` class
* change description of some exceptions
* make `_build_into_packet()` set bound
* export rrPacketsStream
* add `-v` to ci/cd file
##### Pre release, so, v = 1.0.2 + 1.0.1 - pre.1

# Version 1.0.3-pre.2 (Jan 22 2023)
* add something for fix something (about Optional?)
* add new type `FiniteLengthByteArray`
* fix `EncryptionRequest` / `EncryptionResponse` structures by rewriting it with new type
* add some new classifiers to `setup.py`
##### Pre release, so, v = 1.0.3 - pre.2

# Version 1.0.3 (Jan 26 2023)
* fix `ValueError` description on abs packet initing
* make `Optional` be able to work reverse
* add new type `FiniteLengthArray`
* add new type `UUID`
* now protocol version number can be accessed on `cubelib.proto.v??.version`
* add new packets to `v47.ClientBound.Play`
* add autotests

# Version 1.0.4-pre1 (Jan 29 2023)
* add new cli tool `mcsr` created for minecraft server status retrieving
* add code base for minecraft clients

# Version 1.0.4 (Mar 10 2023)
* remove code base for minecraft clients
* add async minecraft server status retriever class `AsyncStatusRetriever`
* rewrite `mcsr` cli tool
* add more typing in `cubelib.types`

# version 1.0.5-pre1 (Mar 17 2023)  MCSR UPDATE
* add online players displaying to mcsr
* add ability to run mscr without pil
* add `AsyncStatusRetriever.retrieve()` return parameter: `used protocol` 2 = ipv4, 23 = ipv6

# version 1.0.7 (June 5 2023) ГАРНОГО ЛІТА
* `NextState` is deprecated. use `cubelib.state` instead in `Handshaking.NextState`
* add Postition type def (before 1.14)
* add types segregation
* add mcenums segregation
* improve tests
* add materials
* add mobs
* explain exceptions
* add new reqirement - PYPI:NBT
* add tests segregation
* expand some errors' description

# version 1.0.8 (June 19 2023)
* fix unlisted in pypi dependency (nbt)
* fix broken equality check in case of two packet with disabled options
* add tests for fixed optionals eqality
* add fixed point type and replace its usages definitions
* rename some objects (v47)

# version 1.0.9 (Jul 8 2023)
* fix issue #3
* add test for new fix
* add v47 sounds enum
* add json type
* remove mistaken `print()` from `Night.__eq__`
* fix typo in `mcsr.py`

# version 1.0.10-pre1 (Mar 17 2024)
* add chunk_examples
* add chat element support, export Chat, ChatMessagePositionEnum
* add SafeBuff strictly_exhausted: bool
* fix some type annotations
* add world primitives
* add proto capabilities
* add v47 chunk read/write support
* update v47 materials/blocks
* add tests for chat
* add tests for v47 world

