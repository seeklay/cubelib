import json
import unicodedata
from cubelib.chat import Chat


def test_chat_from_old():
    old_desc = "§6[§e§l§nTesla§a§l§nCraft§b§l§n.org§6]                                   §6[§a1.12.2§6] §6[§aПвП 1.8§6]\n§e§l150 новых арен БедВарс!"
    chat = Chat.from_old(old_desc)
    ansi = chat.ansi()
    print(json.dumps(chat.json, indent=4))
    print(f"\n{ansi}")

def test_chat_loads():
    player_join = {"extra":[{"color":"yellow","text":"seeklay joined the game"}],"text":""}
    chat = Chat.loads(player_join)
    ansi = chat.ansi()
    print(json.dumps(chat.json, indent=4))
    print(f"\n{ansi}")

def test_chat_inherit():
    inherit_test = {"text":"foo","color":"red","extra":[{"text":"bar"},{"text":"baz","color":"white"},{"text":"qux"}]}
    chat = Chat.loads(inherit_test)
    ansi = chat.ansi()
    print(json.dumps(chat.json, indent=4))
    print(f"\n{ansi}")

def test_chat_say():
    chat_say = {"translate":"chat.type.announcement","with":[{"insertion":"seeklay","clickEvent":{"action":"suggest_command","value":"/msg seeklay "},"hoverEvent":{"action":"show_entity","value":"{name:\"seeklay\",id:\"214ffe35-8308-30a6-a684-0ddad26cb75f\"}"},"text":"seeklay"},{"extra":["me"],"text":""}]}
    chat = Chat.loads(chat_say)
    ansi = chat.ansi()
    print(json.dumps(chat.json, indent=4))
    print(f"\n{ansi}")

def test_chat_me():
    chat_me = {"translate":"chat.type.emote","with":[{"insertion":"seeklay","clickEvent":{"action":"suggest_command","value":"/msg seeklay "},"hoverEvent":{"action":"show_entity","value":"{name:\"seeklay\",id:\"214ffe35-8308-30a6-a684-0ddad26cb75f\"}"},"text":"seeklay"},{"extra":["aaa"],"text":""}]}
    chat = Chat.loads(chat_me)
    ansi = chat.ansi()
    print(json.dumps(chat.json, indent=4))
    print(f"\n{ansi}")

def test_chat_msg_old():
    chat_old = {"extra":["\u003cseeklay\u003e gfvfdgdf"],"text":""}
    chat = Chat.loads(chat_old)
    ansi = chat.ansi()
    print(json.dumps(chat.json, indent=4))
    print(f"\n{ansi}")

def test_chat_msg_new(chat_new):
    chat = Chat.loads(chat_new)
    ansi = chat.ansi()
    print(json.dumps(chat.json, indent=4))
    print(f"\n{ansi}")

def test_chat_server_desc(server_desc):
    chat = Chat.loads(server_desc)
    ansi = chat.ansi()
    print(json.dumps(chat.json, indent=4))
    ansi = unicodedata.normalize('NFKC', ansi)
    print(f"\n{ansi}")

