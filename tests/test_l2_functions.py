import cubelib
import cubelib.proto.v47
import cubelib.proto.v340
import cubelib.mcenums as mcenums

def  test_packets_equality():
    """
    Test that __eq__() works right
    """

    # p.Night
    assert cubelib.proto.ClientBound.Status.Pong(1) == cubelib.proto.ClientBound.Status.Pong(1)
    assert cubelib.proto.ClientBound.Status.Pong(1) != cubelib.proto.ClientBound.Status.Pong(22)
    assert cubelib.proto.ClientBound.Status.Pong(1) != cubelib.proto.ClientBound.Status.Response('a')
    assert cubelib.proto.ClientBound.Status.Pong(1) != str

    # p.Packet
    assert cubelib.p.Packet(1, b'\x01') == cubelib.p.Packet(1, b'\x01')
    assert cubelib.p.Packet(1, b'\x01') != cubelib.p.Packet(1, b'\x02')
    assert cubelib.p.Packet(1, b'\x01') != cubelib.p.Packet(2, b'\x01')
    assert cubelib.p.Packet(1, b'\x01', bound=cubelib.bound.Client) != cubelib.p.Packet(1, b'\x01', bound=cubelib.bound.Server)
    assert cubelib.p.Packet(1, b'\x01') != str



def test_unclaimed_data_detection():
    """
    Test that end of packet's fields leads buffer exhausting
    """

    try:
        cubelib.p.Packet(2, b'\x01\x41\x42', bound=cubelib.bound.Server).resolve(cubelib.state.Play, cubelib.proto.v340)
        raise Exception(f"We waited for DecoderException, but it not raised. It's not good!")

    except cubelib.errors.DecoderException as e:
        assert e.args[0] == "Packet [ChatMessage] fields over, but packet's buffer not empty yet! Unexpected data len: 1 Data: b'B'\nBroken Packet: Play.ChatMessage(Message='A')"



def test_hdn_resolve():
    """
    Test that Night.resolve() works right
    """

    apacket = cubelib.proto.v340.ServerBound.Play.ChatMessage('text =]')
    packet = cubelib.p.Packet(id=2, payload=b'\x07text =]', bound=cubelib.bound.Server, compressed=False)

    assert packet.resolve(cubelib.state.Play, cubelib.proto.v340) == apacket

    apacket = cubelib.proto.v47.ServerBound.Play.UseEntity(1, mcenums.UseEntityTypeEnum.InteractAt, 1.0, 1.0, 1.0)
    packet = cubelib.p.Packet(id=2, payload=b'\x01\x02?\x80\x00\x00?\x80\x00\x00?\x80\x00\x00', bound=cubelib.bound.Server, compressed=False)

    assert packet.resolve(cubelib.state.Play, cubelib.proto.v47) == apacket

    apacket = cubelib.proto.ClientBound.Status.Response('MOTD')
    packet = cubelib.p.Packet(id=0, payload=b'\x04MOTD', bound=cubelib.bound.Client, compressed=False)

    assert packet.resolve(cubelib.state.Status, cubelib.proto) == apacket

def test_hdn_build_into_packet():
    """
    Test Night._build_into_packet() works right
    """

    apacket = cubelib.proto.v47.ServerBound.Play.UseEntity(1, mcenums.UseEntityTypeEnum.InteractAt , 1.0, 1.0, 1.0)
    prototype = cubelib.p.Packet(id=2, payload=b'\x01\x02?\x80\x00\x00?\x80\x00\x00?\x80\x00\x00', bound=cubelib.bound.Server, compressed=False)

    packet = apacket._build_into_packet()
    assert packet == prototype
