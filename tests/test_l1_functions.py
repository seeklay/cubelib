import cubelib
import cubelib.types as types

def test_read_plain():
    """
    Test plain packet reading
    """

    # v47.ServerBound.Play.KeepAlive
 
    binary = b"\x02\x00\x5e"
    prototype = cubelib.p.Packet(id=0, payload=b"\x5e", bound=cubelib.bound.Server)

    packet, tail = cubelib.p.Packet._read_plain(binary, cubelib.bound.Server)

    assert packet == prototype
    assert tail == b''

    try:
        packet, tail = cubelib.p.Packet._read_plain(b"\xff\xff\xff\xff\xff\xff\x7f", cubelib.bound.Server)
    except cubelib.errors.BadPacketException as e:
        assert e.args[0] == "Invalid packet length field!"

    try:
        packet, tail = cubelib.p.Packet._read_plain(types.VarInt.build(-1), cubelib.bound.Server)
    except cubelib.errors.BadPacketException as e:
        assert e.args[0] == "Packet length field -1 invalid! (2097151 >= length > 0)"
    
    try:
        packet, tail = cubelib.p.Packet._read_plain(b"\x07\xff\xff\xff\xff\xff\xff\x7f", cubelib.bound.Server)
    except cubelib.errors.BadPacketException as e:
        assert e.args[0] == "Invalid packet id field!"

def test_read_compressed():
    """
    Test compressed packet reading
    """

    # proto.ClientBound.Status.Pong

    binary_0 = b'\x0e\tx\x9ccd\x00\x03VK\x00\x00U\x00@' # compressed 'cause packet len (9) > threshold (0)
    binary_256 = b'\n\x00\x01\x00\x00\x00\x00\x00\x00\x059' # not compressed 'cause packet len (9) < threshold (256)
    prototype = cubelib.p.Packet(id=1, payload=b"\x00\x00\x00\x00\x00\x00\x059", bound=cubelib.bound.Client)

    p0, tail0 = cubelib.p.Packet._read_compressed(binary_0, cubelib.bound.Client, 0)
    p256, tail1 = cubelib.p.Packet._read_compressed(binary_256, cubelib.bound.Client, 256)

    assert p0 == p256 == prototype
    assert tail0 == tail1 == b''

    try:
        packet, tail = cubelib.p.Packet._read_compressed(b"\xff\xff\xff\xff\xff\xff\x7f", cubelib.bound.Server, 256)
    except cubelib.errors.BadPacketException as e:
        assert e.args[0] == "Invalid packet length field!"
    
    try:
        packet, tail = cubelib.p.Packet._read_compressed(types.VarInt.build(-1), cubelib.bound.Server, 256)
    except cubelib.errors.BadPacketException as e:
        assert e.args[0] == "Packet length field -1 invalid! (2097151 >= length > 0)"

    try:
        packet, tail = cubelib.p.Packet._read_compressed(b"\x07\xff\xff\xff\xff\xff\xff\x7f", cubelib.bound.Server, 256)
    except cubelib.errors.BadPacketException as e:
        assert e.args[0] == "Invalid data length field!"

    try:
        packet, tail = cubelib.p.Packet._read_compressed(types.VarInt.build(-1)+types.VarInt.build(-1), cubelib.bound.Server, 256)
    except cubelib.errors.BadPacketException as e:
        assert e.args[0] == "Packet length field -1 invalid! (2097151 >= length > 0)"

    try:
        packet, tail = cubelib.p.Packet._read_compressed(b"\x02\x01\x01", cubelib.bound.Server, 256)
    except cubelib.errors.BadPacketException as e:
        assert e.args[0] == "compressed data length (1) is less than the threshold (256)"


def test_build_plain():
    """
    Test plain packet building
    """

    # v47.ServerBound.Play.KeepAlive

    packet = cubelib.p.Packet(id=0, payload=b"\x5e", bound=cubelib.bound.Server)
    prototype = b"\x02\x00\x5e"

    binary = packet.build()

    assert binary == prototype

def test_build_compressed():
    """
    Test compressed packet building
    """

    # proto.ClientBound.Status.Pong

    packet = cubelib.p.Packet(id=1, payload=b"\x00\x00\x00\x00\x00\x00\x059", bound=cubelib.bound.Client)
    prototype_0 = b'\x0e\tx\x9ccd\x00\x03VK\x00\x00U\x00@' # compressed 'cause packet len (9) > threshold (0)
    prototype_256 = b'\n\x00\x01\x00\x00\x00\x00\x00\x00\x059' # not compressed 'cause packet len (9) < threshold (256)

    binary0 = packet.build(0)
    binary256 = packet.build(256)

    assert binary0 == prototype_0
    assert binary256 == prototype_256

def test_glob_readPacketsStream():
    """
    Test that readPacketsStream() works right
    """

    # v47.ServerBound.Play.KeepAlive
    binary = b"\x02\x00\x5e"
    cbinary = b"\x03\x00\x00^"
    prototype = cubelib.p.Packet(id=0, payload=b"\x5e", bound=cubelib.bound.Server)
    threshold = -1 # not compressed

    p = []
    tail = cubelib.readPacketsStream(binary, threshold, cubelib.bound.Server, p)
    assert p == [prototype]
    assert tail == b''

    p = []
    tail = cubelib.readPacketsStream(cbinary, 256, cubelib.bound.Server, p)
    assert p == [prototype]
    assert tail == b''

    p = []
    tail = cubelib.readPacketsStream(binary * 7, threshold, cubelib.bound.Server, p)
    assert p == [prototype] * 7
    assert tail == b''

    p = []
    tail = cubelib.readPacketsStream(binary * 3 + binary[:int(len(binary)/2)], threshold, cubelib.bound.Server, p)
    assert p == [prototype] * 3
    assert tail == binary[:int(len(binary)/2)]

def test_glob_rrPacketsStream():
    """
    Test that rrPacketsStream() works right
    """

    p = []
    cubelib.p.rrPacketsStream(b'\x01\x00', -1, cubelib.bound.Server, cubelib.state.Status, cubelib.proto, p)

    assert p[0] == cubelib.proto.ServerBound.Status.Request()