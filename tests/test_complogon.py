import cubelib
import cubelib.proto.v47
from cubelib.proto import ClientBound
import pytest

def test_complogon():
    """
        Fix Issue #3 readPacketsStream() reads LoginSuccess as uncompressed
        after SetCompression if they are sent in single binary packet/tcp chunk

        fix:
            add max_depth arg to cut maximum parse length if state is Login
    """    

    buffer = ClientBound.ClassicLogin.SetCompression(1000).build() + ClientBound.ClassicLogin.LoginSuccess("214ffe35-8308-30a6-a684-0ddad26cb75f", "seeklay").build(1000)

    with pytest.raises(cubelib.errors.DecoderException):
        unex = cubelib.rrPacketsStream(buffer, -1, cubelib.bound.Client, cubelib.state.Login, cubelib.proto.v47, [])

    with pytest.raises(cubelib.errors.DecoderException):
        unex = cubelib.rrPacketsStream(buffer, -1, cubelib.bound.Client, cubelib.state.Login, cubelib.proto.v47, [], max_depth=2)

    p = []
    unex = cubelib.rrPacketsStream(buffer, -1, cubelib.bound.Client, cubelib.state.Login, cubelib.proto.v47, p, max_depth=1)
    print(unex)
    print(p)
