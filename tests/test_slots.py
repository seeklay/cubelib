"""
My all collection of slottests =] =S ;)
"""

from cubelib.proto import ClientBound, ServerBound
from cubelib.proto import v47, v340

import cubelib
import cubelib.types as types

from nbt.nbt import *  # NBT

def test_slots():
    empty = None
    diam_sword = {"id": v47.Material.DIAMOND_SWORD, "count": 1, "damage": 0}

    nbtf = v47.types.NBT()
    list_ench = TAG_List(name='ench', type=TAG_Compound)

    enchantment = TAG_Compound()

    ench_id = TAG_Short(name="id", value=255)
    ench_lvl = TAG_Short(name="lvl", value=5)
    enchantment.tags.append(ench_id)
    enchantment.tags.append(ench_lvl)

    list_ench.append(enchantment)

    nbtf.tags.append(list_ench)

    diam_sword_nbt = {"id": v47.Material.DIAMOND_SWORD, "count": 1, "damage": 0, "nbt": nbtf}
    p = v47.ServerBound.Play.CreativeInventoryAction(37, empty)
    rs = p._build_into_packet().resolve(cubelib.state.Play, v47)
    assert p == rs

    p = v47.ServerBound.Play.CreativeInventoryAction(37, diam_sword)
    rs = p._build_into_packet().resolve(cubelib.state.Play, v47)
    assert p == rs

    p = v47.ServerBound.Play.CreativeInventoryAction(37, diam_sword_nbt)
    rs = p._build_into_packet().resolve(cubelib.state.Play, v47)
    assert p == rs

    nbtf = v47.types.NBT()

    repair_cost = TAG_Int(name="RepairCost", value=1)
    comp_display = TAG_Compound(name="display")

    name = TAG_String(name="Name", value="Diamond Sword123")
    comp_display.tags.append(name)

    nbtf.tags.append(repair_cost)
    nbtf.tags.append(comp_display)
    #diam_sword_nbt = {"id": v47.Material.DIAMOND_SWORD, "count": 1, "damage": 0, "nbt": nbtf}
    print(nbtf.pretty_tree())
    print(nbtf.build())
