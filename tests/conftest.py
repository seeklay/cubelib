import pytest

@pytest.fixture(scope="session")
def server_desc():
    return {"extra":[{"obfuscated":True,"color":"black","text":"s "},{"color":"white","text":"« "},{"color":"aqua","text":"❄ "},{"color":"white","text":"» "},{"bold":True,"color":"green","text":"ＭＩＧＯＳ"},{"bold":True,"color":"yellow","text":"ＭＣ"},{"color":"gray","text":": "},{"color":"dark_green","text":"1.8-1.19.2 "},{"bold":True,"color":"white","text":"#Обнова "},{"bold":True,"color":"red","text":"#Вайп\n"},{"obfuscated":True,"color":"black","text":"s "},{"strikethrough":True,"color":"dark_gray","text":"-]"},{"strikethrough":True,"color":"aqua","text":"--"},{"color":"white","text":"  "},{"bold":True,"underlined":True,"color":"red","text":"20+ МИНИ-ИГР"},{"color":"white","text":" / "},{"bold":True,"underlined":True,"color":"gold","text":"ЛЕТНИЕ ОБНОВЛЕНИЕ"},{"color":"white","text":" / "},{"color":"white","text":"Работаем "},{"color":"aqua","text":"6 лет"}],"text":""}

@pytest.fixture(scope="session")
def chat_new():
    return {"translate":"chat.type.text","with":[{"text":"Herobrine","clickEvent":{"action":"suggest_command","value":"/msg Herobrine "},"hoverEvent":{"action":"show_entity","value":"{id:f84c6a79-0a4e-45e0-879b-cd49ebd4c4e2,name:Herobrine}"},"insertion":"Herobrine"},{"text":"I don't exist"}]}
