from cubelib.types import * # type: ignore
import cubelib.proto.v47.world
import cubelib.world

from pathlib import Path
import json
import time

pwd = Path(__file__).parent


def test_bulk_default():
    with open(pwd/"assets/world/v47_MapChunkBulk.dat", "rb") as f:
        COLUMNS_DATA = f.read()
    with open(pwd/"assets/world/v47_MapChunkBulk.json", "rb") as f:
        METADATA = json.load(f)
    
    buffer = SafeBuff(COLUMNS_DATA, strictly_exhausted=1)

    column_iter = cubelib.proto.v47.world.load_columns_1(
        buffer,
        METADATA,
        True
    )

    t1 = time.time()
    columns = list(column_iter)
    print(f"{(time.time() - t1)*1000:.2f}ms", "took to parse columns")

    for i, column in enumerate(columns):
        print(f"#{i}", column)

    t1 = time.time()
    re_meta, re_data = cubelib.proto.v47.world.dump_columns_2(
        columns
    )
    print(f"{(time.time() - t1)*1000:.2f}ms", "took to build up")

    assert re_data == COLUMNS_DATA
    assert re_meta == METADATA

def test_bulk_non():
    with open(pwd/"assets/world/v47_MapChunkBulk_LAF.dat", "rb") as f:
        COLUMNS_DATA = f.read()
    with open(pwd/"assets/world/v47_MapChunkBulk_LAF.json", "rb") as f:
        METADATA = json.load(f)
    
    buffer = SafeBuff(COLUMNS_DATA, strictly_exhausted=1)

    column_iter = cubelib.proto.v47.world.load_columns_1(
        buffer,
        METADATA,
        True
    )

    t1 = time.time()
    columns = list(column_iter)
    print(f"{(time.time() - t1)*1000:.2f}ms", "took to parse columns")

    for i, column in enumerate(columns):
        print(f"#{i}", column)

    t1 = time.time()
    re_meta, re_data = cubelib.proto.v47.world.dump_columns_2(
        columns
    )
    print(f"{(time.time() - t1)*1000:.2f}ms", "took to build up")

    assert re_data == COLUMNS_DATA
    assert re_meta == METADATA

def test_single():
    with open(pwd/"assets/world/v47_ChunkData.dat", "rb") as f:
        COLUMNS_DATA = f.read()
    with open(pwd/"assets/world/v47_ChunkData.json", "rb") as f:
        METADATA = json.load(f)
    
    buffer = SafeBuff(COLUMNS_DATA, strictly_exhausted=1)

    column_iter = cubelib.proto.v47.world.load_columns_1(
        buffer,
        METADATA,
        True
    )

    t1 = time.time()
    columns = list(column_iter)
    print(f"{(time.time() - t1)*1000:.2f}ms", "took to parse columns")

    for i, column in enumerate(columns):
        print(f"#{i}", column)

    t1 = time.time()
    re_meta, re_data = cubelib.proto.v47.world.dump_columns_2(
        columns
    )
    print(f"{(time.time() - t1)*1000:.2f}ms", "took to build up")

    assert re_data == COLUMNS_DATA
    assert re_meta == METADATA

def test_uniq_blocks():
    with open(pwd/"assets/world/v47_ChunkData.dat", "rb") as f:
        COLUMNS_DATA = f.read()
    with open(pwd/"assets/world/v47_ChunkData.json", "rb") as f:
        METADATA = json.load(f)
    
    buffer = SafeBuff(COLUMNS_DATA, strictly_exhausted=1)

    column_iter = cubelib.proto.v47.world.load_columns_1(
        buffer,
        METADATA,
        True
    )

    t1 = time.time()
    columns = list(column_iter)
    print(f"{(time.time() - t1)*1000:.2f}ms", "took to parse columns")

    for i, column in enumerate(columns):
        print(f"#{i}", column)


    for block in column.uniq_blocks:
        E = cubelib.proto.v47.enums.Material(
            (block >> 4, block & 0x0f)
        )
        print(E)

def test_block():
    bedrock = cubelib.world.Block(7, 0)
    o = bedrock.to1()
    i = cubelib.world.Block.from1(o)
    assert bedrock == i
