"""
My all collection of metatests =] =S ;)
"""

from cubelib.proto import ClientBound, ServerBound
from cubelib.proto import v47, v340

import cubelib
import cubelib.types as types

def test_meta():
    meta = [
        {
            "index": 0,
            "type": 0,
            "value": 0
        },
        {
            "index": 0,
            "type": 1,
            "value": 0
        },
        {
            "index": 0,
            "type": 2,
            "value": 0
        },
        {
            "index": 0,
            "type": 3,
            "value": 1.0
        },
        {
            "index": 0,
            "type": 4,
            "value": "kaneki"
        },
        {
            "index": 0,
            "type": 7,
            "value": (0, 1, 7)
        }
    ]

    p = v47.ClientBound.Play.EntityMetadata(1337, meta)
    print(p)
    rs = p._build_into_packet().resolve(cubelib.state.Play, v47)
    assert p == rs