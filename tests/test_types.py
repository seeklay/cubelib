import cubelib
import cubelib.proto.v47
import cubelib.mcenums as mcenums
import uuid

import pytest

def test_optional_types():
    """
    Test Optional special type
    """

    """
    # UseEntity one of packet's using Optional
    class UseEntity(Night):

        Target: VarInt
        Type: VarInt
        TargetX: Optional('Type', 2)[Float]
        TargetY: Optional('Type', 2)[Float]
        TargetZ: Optional('Type', 2)[Float]
    """
    apacket = cubelib.proto.v47.ServerBound.Play.UseEntity(1, mcenums.UseEntityTypeEnum.InteractAt, 1.0, 1.0, 1.0)
    packet = apacket._build_into_packet()
    npacket = packet.resolve(cubelib.state.Play, cubelib.proto.v47)
    assert apacket == npacket

    apacket = cubelib.proto.v47.ServerBound.Play.UseEntity(1, mcenums.UseEntityTypeEnum.Attack)
    packet = apacket._build_into_packet()

    opt = cubelib.types.Optional("A", (2, 3), equals=False)
    assert opt.is_legit(1) == True
    assert opt.is_legit(2) == False

    opt = cubelib.types.Optional("A", 2, equals=False)
    assert opt.is_legit(1) == True
    assert opt.is_legit(2) == False

    opt = cubelib.types.Optional("A", (1, 2))
    assert opt.is_legit(1) == True
    assert opt.is_legit(3) == False

    # check disabled optionals equality (after fix)
    t = cubelib.proto.v47.Object.BOAT
    p1 = cubelib.proto.v47.ClientBound.Play.SpawnObject(1, t, 2, 3, 4, 5, 5, 0)
    p2 = cubelib.proto.v47.ClientBound.Play.SpawnObject(1, t, 2, 3, 4, 5, 5, 0)
    
    assert p1 == p2

def test_new_enums():
    p = cubelib.proto.v47.ServerBound.Play.PlayerDigging(cubelib.proto.v47.ServerBound.Play.PlayerDigging.Status.FinishedDigging, (-1,-1,-1), mcenums.FaceEnum.YNEG)
    pp = p._build_into_packet()
    pp.resolve(cubelib.state.Play, cubelib.proto.v47)

    p = cubelib.proto.v47.ServerBound.Play.EntityAction(1, cubelib.proto.v47.ServerBound.Play.EntityAction.ActionID.StartSneaking, 1)
    pp = p._build_into_packet()
    pp.resolve(cubelib.state.Play, cubelib.proto.v47)

def test_pli():
    pp = [
        {
            'name': 'textures',
            'value': 'ewogICJ0aW1lc3RhbXAiIDogMTY1OTkzMjUyNjcyNCwKICAicHJvZmlsZUlkIiA6ICJhYzM2YmVkZGQxNGQ0YjVmYmQyYzc5OThlMWMwOTg3ZCIsCiAgInByb2ZpbGVOYW1lIiA6ICJtYWlzYWthIiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2JmYmQzMTBlMGQ4NTQxMTBiZTgxYWQwNWU0OGRjYTRiN2ZjNGIxNGJmYWI2ODc4MDcwOGE3NmEwZDMxNjk5N2QiLAogICAgICAibWV0YWRhdGEiIDogewogICAgICAgICJtb2RlbCIgOiAic2xpbSIKICAgICAgfQogICAgfQogIH0KfQ==',
            'signed': True,
            'signature': 'Ba4gDtCUKez8zmNQCSuov8iBVOLG5iq/M6Gfc4sSSsbWLKa3IVzs8+E+qZXLD4GUajUDS9iuTXshXJ9put/k47pOMk1AtyZTvX+QGMlnw48rPiinSMBoiexdfYRF4VEzvMMYO5A7jtJeSdVBP6RBXT0sP1BLJl5/FtGu/RX+pGvtsE0ffezDd5vyAuTiSUz4Ug/6PyQpdeeuuruE/UDsGR0LSZIX1KeyVgmsWVLDLSiS+3EeQDhK7Cso1FeZF85QIIINQNrFWsvtTSPrafAlySZpUBXwte1hm/FfgjTrt1bL8Zj2thcOKXG9ePaQbfXrcqd1ZYktsl+hqpC765Hl+II3A4Iz5z0EBJIP3A6KNKl1B3FUDFaoiTQ8ZXlH60X2ENuBBvDOylqKH/tUaXPWnK/47gaCQIaRvt9VJbdYQtvD6Ez8Innpk2XbNuLGY9MMs1Fa46JymZH3B+EWFv9LmxsL9Ln7nEZvUsDXuAAr11VT8JVkriWAr+bAq4T4kP8QExyMiflO3JuIcF2GmbV1AcsabH2gRaQq9iDGuTNj+jt0rJl3flUmngFSmoBewy1XrnhpfAMwnrBABdO03jgaiic/9nX8WpJRWp2V1RIeqJJXs7boSCETwd/LNCh7ITsvDoL5SJRki+/zpg/SDBOaKbWTlKY27znyHe7iDBCBsYk='
        }
    ]
    data = {
    "action": cubelib.types.PlayerListItemData.Action.AddPlayer,
    "players": [
            {
                "uuid": uuid.UUID(bytes=b'!O\xfe5\x83\x080\xa6\xa6\x84\r\xda\xd2l\xb7_'),
                "name": "seeklay",
                "properties": pp,
                "gamemode": 0,
                "ping": 0,
                "display_name": "nooop"
            }
        ]
    }

    #test add
    p = cubelib.proto.v47.ClientBound.Play.PlayerListItem(data)
    print(p._build_into_packet().resolve(cubelib.state.Play, cubelib.proto.v47))

    data = {
        "action": cubelib.types.PlayerListItemData.Action.RemovePlayer,
        "players": [{"uuid": uuid.UUID(bytes=b'!O\xfe5\x83\x080\xa6\xa6\x84\r\xda\xd2l\xb7_')}]    
    }

    #test remove
    p = cubelib.proto.v47.ClientBound.Play.PlayerListItem(data)
    print(p._build_into_packet().resolve(cubelib.state.Play, cubelib.proto.v47))

    #test gamemode update
    data["action"] = cubelib.types.PlayerListItemData.Action.UpdateGamemode
    data["players"][0]["gamemode"] = 1
    p = cubelib.proto.v47.ClientBound.Play.PlayerListItem(data)
    print(p._build_into_packet().resolve(cubelib.state.Play, cubelib.proto.v47))

    #test latency update
    data["action"] = cubelib.types.PlayerListItemData.Action.UpdateLatency
    data["players"][0]["ping"] = 90
    p = cubelib.proto.v47.ClientBound.Play.PlayerListItem(data)
    print(p._build_into_packet().resolve(cubelib.state.Play, cubelib.proto.v47))

    #test disaplayname update
    data["action"] = cubelib.types.PlayerListItemData.Action.UpdateDisplayName
    data["players"][0]["display_name"] = "greg"
    p = cubelib.proto.v47.ClientBound.Play.PlayerListItem(data)
    print(p._build_into_packet().resolve(cubelib.state.Play, cubelib.proto.v47))

def test_nbt():
    nbt_bytes = b'\n\x00\x00\x03\x00\nRepairCost\x00\x00\x00\x01\n\x00\x07display\x08\x00\x04Name\x00\x10Diamond Sword123\x00\x00'
    nbt_buffer = cubelib.types.SafeBuff(nbt_bytes)
    nbt_file = cubelib.types.NBT.destroy(nbt_buffer)
    nbt_dict = nbt_file.to_dict()
    assert {'': {'RepairCost': ('int', 1), 'display': {'Name': 'Diamond Sword123'}}} == nbt_dict

    nbt_file = cubelib.types.NBT.from_dict(nbt_dict)
    assert nbt_bytes == nbt_file.build()

    nbt_dict = {
        'Devel': {
            "Strs": ('str', [
                "Terve",
                "Hello",
                "Privet",
                "Konnichiha"
            ]),

            "Lsts": ('list', [
                ('long', [('long', 1), ('long', 2), ('long', 3)]),
                ('dict', [
                    {
                        "name": {"name": "name"},
                        "value": {"value": ('str', ["value"])},
                        "Just int": ("int", 1)
                    }
                ]) 
            ]),
            "Mina olen": "cubelib"
        }
    }
    nbt_file = cubelib.types.NBT.from_dict(nbt_dict)
    assert nbt_file != 1
    str(nbt_file)
    assert nbt_dict == nbt_file.to_dict()
    assert cubelib.types.NBT.from_dict(nbt_dict) == nbt_file    


def test_fla():
    str_arr = ['a', 'b', 'c']
    fla = cubelib.types.FiniteLengthArray(cubelib.types.VarInt)[cubelib.types.String]
    fla_bytes = fla.build(str_arr)

    assert str_arr == fla.destroy(cubelib.types.SafeBuff(fla_bytes))

    with pytest.raises(RuntimeError):
        fla.destroy(cubelib.types.SafeBuff(fla_bytes[:-1]))
