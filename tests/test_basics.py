"""
My small collection of tests =] =S ;)
"""

"""
from cubelib.proto import ClientBound, ServerBound
from cubelib.proto import v47, v340

import cubelib
import cubelib.types as types
import cubelib.mcenums as enums

import uuid
from nbt.nbt import * # NBT
Mob = v47.enums.Mob





def globaltest(obj):
#        Testing all packets in map
    

    nbtf = v47.types.NBT()
    repair_cost = TAG_Int(name="RepairCost", value=1)
    comp_display = TAG_Compound(name="display")    
    name = TAG_String(name="Name", value="Diamond Sword123")
    comp_display.tags.append(name)
    nbtf.tags.extend([repair_cost, comp_display])    
    
    Sample = {
        cubelib.types.VarInt: 1,
        cubelib.types.Int: 1,
        cubelib.types.String: 'a',
        cubelib.types.Bool: True,
        cubelib.types.Double: 1,
        cubelib.types.Float: 1.0,
        cubelib.types.Byte: 1,
        cubelib.types.Position: (-1, -1, -1),
        cubelib.types.Slot: b"\x41",
        cubelib.types.Short: 1,
        cubelib.types.UnsignedByte: 1,
        cubelib.types.ByteArray: b"\x41",
        cubelib.types.Long: 1,
        cubelib.types.UUID: uuid.uuid4(),
        cubelib.types.Angle: 1,
        cubelib.types.UnsignedShort: 1,
        cubelib.NextState: cubelib.NextState.Login,
        cubelib.types.FiniteLengthByteArray: b"\x01",
        
        #cubelib.types.PlayerDiggingStatusEnum: cubelib.types.PlayerDiggingStatusEnum.FinishedDigging,
        #cubelib.types.FaceEnum: cubelib.types.FaceEnum.YNEG,
        #cubelib.types.EntityActionActionIDEnum: cubelib.types.EntityActionActionIDEnum.StartSneaking,
        cubelib.types.Metadata: {'index': 1, 'value': 1},
        cubelib.types.Property: {"key": 1, "value": 1, "modifiers": 1},
        cubelib.types.BlockID: v47.types.BlockID(v47.Material.AIR, 0),
        cubelib.types.PlayerListItemData: {'action': cubelib.proto.v47.types.PlayerListItemData.Action.AddPlayer, 'players': [{'uuid': uuid.uuid4(), "name": "seeklay", 'properties': [], 'gamemode': 0, 'ping': 0, 'displayname': None}]},
        cubelib.types.Slot: {"id": v47.Material.AIR, "count": 1, "damage": 0},
        cubelib.types.ChunkMeta: (-1, -1, 15),
        cubelib.types.NBT: nbtf,
        cubelib.types.StatisticsElement: {'name': 'achievement.openInventory', 'value': 40},
        #cubelib.types.UseEntityTypeEnum: cubelib.types.UseEntityTypeEnum.InteractAt,
        Mob: Mob.WITHER,
        #cubelib.types.UpdateBlockEntityActionEnum: cubelib.types.UpdateBlockEntityActionEnum.Spawner,
        #cubelib.types.ScoreboardObjectiveModeEnum: cubelib.types.ScoreboardObjectiveModeEnum.Create,
        #cubelib.types.UpdateScoreActionEnum: cubelib.types.UpdateScoreActionEnum.Update,
        #cubelib.types.ChatMessagePositionEnum: cubelib.types.ChatMessagePositionEnum.Chat,
        #cubelib.types.ClientSettingsChatModeEnum: cubelib.types.ClientSettingsChatModeEnum.Enabled,
        #cubelib.types.ChangeGameStateReasonEnum: cubelib.types.ChangeGameStateReasonEnum.InvalidBed
    }

    for a in obj:        
        if not obj[a] or not hasattr(obj[a], '__annotations__'):
            continue

        an = obj[a].__annotations__        
        args = []
        for e in an:
            x = an[e]
            if isinstance(x, cubelib.types.String):
                t = Sample[x.__class__]
            elif isinstance(x, cubelib.types.Optional):
                t = Sample[x.type]
            elif isinstance(x, cubelib.types.FiniteLengthArray):                            
                for i in Sample:
                    if issubclass(x.TYPE, i):
                        print("!!!", x.TYPE, i)
                        t = [Sample[i]]
                        break
            else:
                for i in Sample:
                    if issubclass(x, i):
                        t = Sample[i]
                        break
                #t = Sample[x]
            args.append(t)
        
        print("->>>", *args)
        abstract_packet = obj[a](*args)
        print(abstract_packet)
        packet_packet = abstract_packet._build_into_packet()
        #print(packet_packet)
        #binary_packet = packet_packet.build()
        #print(boinary_packet)

def test_vind_ServerBound():

    globaltest(cubelib.proto.ServerBound.Handshaking.map)
    globaltest(cubelib.proto.ServerBound.Status.map)
    globaltest(cubelib.proto.ServerBound.ClassicLogin.map)

def test_vind_ClientBound():
    
    globaltest(cubelib.proto.ClientBound.Status.map)
    globaltest(cubelib.proto.ClientBound.ClassicLogin.map)
def test_47_ServerBound():

    globaltest(cubelib.proto.v47.ServerBound.Play.map)

def test_47_ClientBound():

    globaltest(cubelib.proto.v47.ClientBound.Play.map)

def test_340_ServerBound():

    globaltest(cubelib.proto.v340.ServerBound.Play.map)

def test_340_ClientBound():

    globaltest(cubelib.proto.v340.ClientBound.Play.map)

    


"""