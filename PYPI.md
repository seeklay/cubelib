# cubelib

Minecraft protocol library for [Python](https://www.python.org/).
  
![](https://gitlab.com/seeklay/cubelib/badges/main/pipeline.svg) ![coverage report](https://gitlab.com/seeklay/cubelib/badges/main/coverage.svg) ![](https://tokei.rs/b1/gitlab/seeklay/cubelib) [![Latest Release](https://gitlab.com/seeklay/cubelib/-/badges/release.svg)](https://gitlab.com/seeklay/cubelib/-/releases)
![]( https://img.shields.io/badge/Made%20with-Python-1f425f.svg ) ![]( https://img.shields.io/gitlab/license/seeklay/cubelib.svg) 

![](https://gitlab.com/seeklay/cubelib/-/raw/main/insteadoflogo.png)   

## About

**cubelib** is designed to be a usable and easy to extend library that provides a high-level abstraction above Minecraft protocol. 

# [Visit project homepage on gitlab for docs/examples](https://gitlab.com/seeklay/cubelib)
