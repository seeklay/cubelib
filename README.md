# cubelib

Minecraft protocol library for [Python](https://www.python.org/).
  
[![](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/) [![](https://img.shields.io/gitlab/license/seeklay/cubelib.svg)](LICENSE/) [![](https://tokei.rs/b1/gitlab/seeklay/cubelib)](#) [![](https://gitlab.com/seeklay/cubelib/badges/main/pipeline.svg)](https://gitlab.com/seeklay/cubelib/-/pipelines) [![](https://gitlab.com/seeklay/cubelib/badges/main/coverage.svg)](https://coveralls.io/gitlab/seeklay/cubelib) [![](https://badgen.net/pypi/v/cubelib) ![](https://img.shields.io/pypi/dw/cubelib?style=flat&logo=pypi)](https://pypi.org/project/cubelib/) [![](https://badgen.net/gitlab/release/seeklay/cubelib)](https://gitlab.com/seeklay/cubelib/-/releases)

![](insteadoflogo.png)   

### У этого файла есть версия на русском языке [**тут**](ПРОЧТИМЕНЯ.md).

## About
**cubelib** is designed to be a usable and easy to extend library that provides a high-level abstraction above Minecraft protocol. 

## Protocol Versions Implementation Status

|Protocol|Minecraft|ClientBound|ServerBound|
|--|--|--|--|
|47|1.8 - 1.8.9|100%|100%|
|340|1.12.2|Chat|Chat|

> This section contains info only about **Play** protocol state support
> **Handshaking, Status, Login** states are version-independent and 
> and supported at 100%  *(Login state was unchanged up to 1.19)*

## Features

* Building/Parsing plain packets

* Building/Parsing compressed packets

* Resolve packets into usable python abstractions

* Easy-to-use abstractions for any packets

* Cool pythonic game data representation. Entities, Mobs, Objects.

## Requirements

Python 3.7 and above (tested and developed under `Python 3.7.9`)

[PyPi - NBT](https://pypi.org/project/NBT/)

## MCSR
> MCSR is a tool for retrieving minecraft server status

**MCSR uses PIL for server icon displaying. You should install PIL using this command if you wanna see server icon:**

`pip install Pillow`

```bash
usage: mcsr [-h] [--proto 47] [--high-res] [--raw] [--iconless] host

Minecraft Server Status Retriever v0.3.0

positional arguments:
  host        Minecraft server addr like mc.hypixel.net:25565 or 127.0.0.1 or
              [::1], can be with port after `:` or not

optional arguments:
  -h, --help  show this help message and exit
  --proto 47  Protocol version field value sent in Handshake packet
  --high-res  If passed, prints full resolution server icon
  --raw       If passed, prints raw server status response excluding the icon
  --iconless  If passed, disables server icon printing
```
### Usage example:
#### MCSR
![](mcsr_usage.png)
#### In-game view
![](mcsr_ingame_view.png)

## Installation

cubelib is published in [PyPI](https://pypi.org/project/cubelib/), so latest release can be installed with one simple command:

```bash
pip install -U cubelib
```

or bleeding edge from git sources (unstable) (may not work at all!):

```bash
git clone https://gitlab.com/seeklay/cubelib.git
cd cubelib/
pip install .
```

## Linked projects

This is a list of projects using cubelib.
* [MCRP](https://gitlab.com/seeklay/MCRP) Minecraft Rewrite Proxy, at beginning was a part of cubelib but now it separate project

> If you developing something great using cubelib, tell us we will glad to add your project here =]

## Usage examples

> cubelib still not have huge documentation. you will learn how to write code by looking at existing examples check out linked projects and docs =S

## Docs

Visit latest cubelib wiki [here](https://cubelib.seeklay.pp.ua/)
*<-New cool site!*

## Contact

##### GitLab

The preferred method of communication is via this GitLab page.
<br>
Open issues, create merge requests and have fun! =]

## Author

### [seeklay](https://gitlab.com/seeklay)

## License

[GNU GPL 3.0](LICENSE)

## Contributing
You can help project by adding support for more protocol versions.
<br>
How to do it you can see in contributing.md that i not created yet =(
